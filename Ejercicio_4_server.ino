#include <SPI.h>
#include <WiFi101.h>
#include "arduino_secrets.h"
#include <aREST.h>
#include <Servo.h>

char ssid[] = SECRET_SSID; // el nombre de tu red wifi
char pass[] = SECRET_PASS; // la contraseña de tu red wifi
int keyIndex = 0;  // El key Index number de tu red (nesesario solo para WEP)

int status = WL_IDLE_STATUS;
WiFiServer server(80); //Indica el puerto del servidor que atendera las conexiones
// Create aREST instance
WiFiClient client;
aREST rest = aREST();
//Se especifica en que pin estara el led, y el servo
#define pinLed 6
#define pinServo 4
Servo myservo;
//Declar functions to be exposed to the API
int ledControl(String tiempo);
int servoControl(String posicion);

void setup() {
  Serial.begin(9600);  // Inicia la comunicacion serie 
  pinMode(pinLed, OUTPUT); // configura el pin 6 como salida
  myservo.attach(pinServo);

  rest.function("ledcontrol",ledControl);
  rest.function("servocontrol",servoControl);

  // Give name and ID to device (ID should be 6 characters long)
  rest.set_id("1");
  rest.set_name("mkr1000");
  
  if (WiFi.status() == WL_NO_SHIELD) {  // Si no se detecta el hardware wifi no se continua
    Serial.println("WiFi shield not present");
    while (true);  // No continua, se queda en el while
  }
  
  while ( status != WL_CONNECTED) {  // intenta conectarse a la red wifi
    Serial.print("Intentando conectarse a la red: ");
    Serial.println(ssid);  // imprime el nombre de la red (SSID);

    // conecta a una red WPA/WPA2. Hay que cambiar esta linea si se quiere conectar a una red WEP
    status = WiFi.begin(ssid, pass);  // se conecta a la red wifi, devuelve el estado de la conexion
    delay(10000);  // espera 10 segundos por la coneccion
  }
  server.begin();  // inicia el servidor web en el puerto especificado
  printWiFiStatus();  // ya conectado se imprime el estatus
}

void loop() {
  client = server.available();   // Pregunta por la clientes en el servidor
   if (!client) {
      return;
    }
    while(!client.available()){
      delay(1);
    }
    rest.handle(client);
}

void printWiFiStatus() {
  // Imprime el SSID de la red a la cual estas conectado:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());
  // imprime la direccion IP de la placa o shield con que estas conectado:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  // Imprime la intensidad de la señal recibida:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

// para utilizar esta función hay que realizar una peticion GET a http://192.168.x.x/ledcontrol?tiempo=x
int ledControl(String tiempo){
  
  int tiempoInt = tiempo.toInt();
  String mensaje = "Se encedera el led por "+tiempo+"ms";
  Serial.println(mensaje);
  digitalWrite(pinLed, true);
  delay(tiempoInt);
  digitalWrite(pinLed, false);
  return 1;
   
}
// para utilizar esta función hay que realizar una peticion GET http://192.168.x.x/servocontrol?posicion=x
int servoControl(String posicion){
  int posicionInt = posicion.toInt();
  String mensaje = "El servo se posicionara en "+posicion + " grados";
  Serial.println(mensaje);
  if(posicionInt>= 0 && posicionInt <= 180)
    myservo.write(posicionInt);  

  return 1;
}
